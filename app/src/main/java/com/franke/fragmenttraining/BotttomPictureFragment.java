package com.franke.fragmenttraining;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BotttomPictureFragment extends Fragment {

    private static TextView topTextView;
    private static TextView bottomTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //attach the xml layout to class
        View view = inflater.inflate(R.layout.bottom_picture_fragment,container,false);

        //creating references to element of view

        topTextView = (TextView) view.findViewById(R.id.top_text_view);
        bottomTextView = (TextView) view.findViewById(R.id.bottom_text_view);

        return view;

    }

    public void setText(String top, String bottom)
    {
        topTextView.setText(top);
        bottomTextView.setText(bottom);

    }


}
