package com.franke.fragmenttraining;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TopSectionFragment extends Fragment {


    private static EditText topText;
    private static EditText bottomText;

    //interface
    TopSectionListener activityCommander;

    //we make sure, that this method will be created
    public interface TopSectionListener
    {
        void createMeme(String top, String bottom);
    }

    //occuring when attaching to activity
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try
        {
            activityCommander = (TopSectionListener) activity;
        }
        catch(ClassCastException ex)
        {
            throw new ClassCastException(activity.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //attach the xml layout to class
        View view = inflater.inflate(R.layout.top_section_fragment,container,false);


        topText=(EditText) view.findViewById(R.id.top_text_input);
        bottomText=(EditText) view.findViewById(R.id.bottom_text_input);

        final Button createButton = (Button) view.findViewById(R.id.create_button);

        //create listener
        createButton.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        buttonClicked(v);
                    }
                }
        );


        return view;

    }

    //handling button click
    public void buttonClicked(View view)
    {

        //if both edittexts are empty
        if(topText.getText().toString().isEmpty() && bottomText.getText().toString().isEmpty())
            Toast.makeText(getActivity(),"@string/empty_text_warning",Toast.LENGTH_LONG).show();
        else
            activityCommander.createMeme(topText.getText().toString(),bottomText.getText().toString());


    }
}
