package com.franke.fragmenttraining;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements TopSectionFragment.TopSectionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    //this is called, when clicking button
    @Override
    public void createMeme(String top, String bottom) {

        BotttomPictureFragment bpf = (BotttomPictureFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_bottom);
        bpf.setText(top,bottom);
    }

}
